import { Router } from 'express';
import ProductRouter from '@modules/products/routes/Product.routes'


const mainRoutes = Router();

mainRoutes.use('/products', ProductRouter);



export default mainRoutes;
