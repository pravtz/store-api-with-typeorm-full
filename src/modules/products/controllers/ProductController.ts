import { Request, Response } from 'express'
import ListProductService from '../services/ListProductService'
import CreateProductService from '../services/CreateProductService'
import ShowProductService from '../services/ShowProductService'
import UpdateProductService from '../services/UpdateProductService'
import DeleteProductService from '../services/DeleteProductService'

export default class ProductController {

  public async index(request: Request, response: Response): Promise<Response> {
    const listProductService = new ListProductService
    const listProduct = await listProductService.execute()

    console.log(listProduct)
    return response.json(listProduct)
  }

  public async store(request: Request, response: Response): Promise<Response> {
    const {name, price, quantity} =  request.body

    const createProductService = new CreateProductService
    const createProduct = await createProductService.execute({name, price, quantity});

    return response.json(createProduct)
  }

  public async show(request: Request, response: Response): Promise<Response> {
    const {id} = request.params

    const showProductService = new ShowProductService
    const showProduct = await showProductService.execute({id})

    return response.json(showProduct)

  }

  public async update(request: Request, response: Response): Promise<Response> {
    const {id} = request.params
    const {name, price, quantity} = request.body

    const updateProductService = new UpdateProductService
    const updateProduct = await updateProductService.execute({id, name, price, quantity});

    return response.json(updateProduct)
  }

  public async delete(request: Request, response: Response): Promise<Response> {
    const {id} = request.params

    const deleteProductService = new DeleteProductService
    const deleteProduct = await deleteProductService.execute({id})

    return response.json([])
  }

}
