import AppErrors from '@shared/errors/App.errors';
import { getCustomRepository } from 'typeorm'
import { Product } from '../typeorm/entities/Product';
import { ProductRepository } from '../typeorm/repositories/ProductRepository'

interface IRequest {
  id: string;
}

class DeleteProductService {
  public async execute({id}:IRequest): Promise<boolean>{
    const productRepository = getCustomRepository(ProductRepository);

    const product = await productRepository.findOne(id);

    if(!product){
      throw new AppErrors('Product not found')
    }

    const result = await productRepository.remove(product)

    if(!result) {
      throw new AppErrors('product cannot be deleted')
    }

    return true;



  }
}

export default DeleteProductService
