import AppErrors from '@shared/errors/App.errors';
import { getCustomRepository } from 'typeorm'
import { Product } from '../typeorm/entities/Product';
import { ProductRepository } from '../typeorm/repositories/ProductRepository'

interface IRequest {
  name: string;
  price: number;
  quantity: number;
}

class CreateProductService {
  public async execute({name, price, quantity}: IRequest): Promise<Product>{
    const productRepository = getCustomRepository(ProductRepository);
    const productExists = await productRepository.findByName(name);

    if(productExists){
      throw new AppErrors('There is already one product with this name!', 401)
    }

    const product = productRepository.create({
      name,
      price,
      quantity
    })

    await productRepository.save(product);

    return product
  }
}

export default CreateProductService
