import AppErrors from '@shared/errors/App.errors';
import { getCustomRepository } from 'typeorm'
import { Product } from '../typeorm/entities/Product';
import { ProductRepository } from '../typeorm/repositories/ProductRepository'

interface IRequest {
  id: string;
  name: string;
  price: number;
  quantity: number;
}

class UpdateProductService {
  public async execute({id, name, price, quantity}: IRequest): Promise<Product>{
    const productRepository = getCustomRepository(ProductRepository);

    const product = await productRepository.findOne(id);
    if(!product){
      throw new AppErrors('product not found')
    }

    const productNameExists = await productRepository.findByName(name);
    if(productNameExists){
      throw new AppErrors('There is already one product with this name!')
    }

    product.name = name;
    product.price = price;
    product.quantity = quantity;


    await productRepository.save(product);

    return product
  }
}

export default UpdateProductService
