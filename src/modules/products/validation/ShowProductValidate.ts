import { celebrate, Segments, Joi } from "celebrate"

const ShowProductValidate = celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    id: Joi.string().uuid().required(),
  })
})

export default ShowProductValidate
