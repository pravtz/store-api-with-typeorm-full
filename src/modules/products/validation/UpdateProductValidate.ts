import { celebrate, Segments, Joi } from "celebrate"

const UpdateProductValidate = celebrate({
  [Segments.BODY]: Joi.object().keys({
    name: Joi.string().required(),
    price: Joi.number().required(),
    quantity: Joi.number().required()
  }),
  [Segments.PARAMS]: Joi.object().keys({
    id: Joi.string().uuid().required(),
  })
})

export default UpdateProductValidate
