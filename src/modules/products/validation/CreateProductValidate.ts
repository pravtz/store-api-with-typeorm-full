import { celebrate, Segments, Joi } from "celebrate"

const CreateProductValidate = celebrate({
  [Segments.BODY]: Joi.object().keys({
    name: Joi.string().required(),
    price: Joi.number().required(),
    quantity: Joi.number().required()
  }),
})

export default CreateProductValidate
