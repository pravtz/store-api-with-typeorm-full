import { Router } from 'express'
import ProductController from '../controllers/ProductController'
import CreateProductValidate from '../validation/CreateProductValidate';
import DeleteProductValidate from '../validation/DeleteProductValidate';
import ShowProductValidate from '../validation/ShowProductValidate';
import UpdateProductValidate from '../validation/UpdateProductValidate';

const productRouter = Router()
const productControler = new ProductController()

productRouter.get('/', productControler.index);
productRouter.post('/' , CreateProductValidate, productControler.store);
productRouter.post('/:id' ,ShowProductValidate, productControler.show);
productRouter.put('/:id' ,UpdateProductValidate, productControler.update);
productRouter.delete('/:id' ,DeleteProductValidate, productControler.delete);

export default productRouter
